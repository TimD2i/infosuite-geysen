-- Time registration per Sales person
-- ETL Model: XSTR_0
-- /
-- 20190320 TVD:
--	- change dimensions
--	- dynamic db path
--	- added gldm7 project type
-- 20190321 TVD:
--	- Added [Planned Quantity (Base)] to [XSTR_0_TIME_PLANNED]
-- 20190322 TVD:
--  - Change dim 3 and 4
--  - add dim 8
-- 20190325 TVD:
--  - add unit price from [Detailed Timesheet Invoice]
-- 20190529 tvd:
--  - Unit cost km is enkele reis moet x2
-- /

-- Declaration of variables
declare @dsa_db_name                nvarchar(100)
declare @dsa_schema                 nvarchar(100)
declare @DW_db_name                 nvarchar(100)
declare @DW_schema                  nvarchar(100)
declare @ETL_table                  nvarchar(20)

-- Init. of variables
set @dsa_db_name                = (select dsa_db_name                from TemplateGeneralSetup)
set @dsa_schema                 = (select dsa_schema                 from TemplateGeneralSetup)
set @DW_db_name                 = (select dw_db_name                 from TemplateGeneralSetup)
set @DW_schema                  = (select dw_schema                  from TemplateGeneralSetup)
set @ETL_table                  = 'ET_XSDT_0'

-- Read data in to ET Table
EXECUTE
('INSERT INTO ' + @DW_db_name + '.' + @DW_schema + '.' + @ETL_table +
'(DWPERTYP
,DWPER
,XCUID
,XCUIDP
,XGECOM
,XGEKON
,XGESAL
,XGLDM1
,XGLDM2
,XGLDM3
,XGLDM4
,XGLDM5
,XGLDM6
,XGLDM7
,XGLDM8
,XGLDM9
,XGESTA
,XDTDM1
,XDTDM2
,XDTDM3
,XDTDM4
,XDTDM5
,XDTDM6
,XDTDM7

,[XSDT_0_TIME_INVOICED]
,[XSDT_0_UNIT_PRICE]
,[XSDT_0_TOTAL_PRICE]
,[XSDT_0_RESOURCE_COST]
,[XSDT_0_UNIT_COST_KM]
,[XSDT_0_QTY_KM_COST]
)

-- Basic dimensions
Select
''1''
,cast(convert(char(8), trl.[Posting Date], 112) as numeric) as DWDate
,isnull(job.[Bill-to Customer No_],'''') as customerno
,isnull(job.[Bill-to Customer No_],'''') as billtocustomerno
,trl.CompanyNo as company
,''1'' as groupno
,coalesce( IIF(job.[Salesperson Responsible] = '''', ''unknown'', job.[Salesperson Responsible]),'''') as SalesPerson
,isnull(job.[Global Dimension 1 Code],'''') as dim2_segment
,isnull(job.[Global Dimension 2 Code],'''') as dim1_regio
,coalesce( IIF(job.[Salesperson Responsible] = '''', ''unknown'', job.[Salesperson Responsible]),'''') as SalesPerson
,'''' as dim4_project
,'''' as dim5_afdeling
,'''' as dim6_kostenplaats
,'''' as dim7_Verworpen_uitgaven
,isnull(job.[Job Type],'''') as dim8_project_type
,isnull(job.[No_],'''') as dim9_project_nr
,
CASE WHEN job.[Bill-to Country_Region Code] = ''BE'' THEN
	CASE
		WHEN job.[Bill-to Post Code] between 1000 and 1300 THEN ''Brussel''
		WHEN job.[Bill-to Post Code] between 1300 and 1500 THEN ''Waals-Brabant''
		WHEN job.[Bill-to Post Code] between 1500 and 2000 THEN ''Vlaams-Brabant''
		WHEN job.[Bill-to Post Code] between 2000 and 3000 THEN ''Antwerpen''
		WHEN job.[Bill-to Post Code] between 3000 and 3500 THEN ''Vlaams-Brabant''
		WHEN job.[Bill-to Post Code] between 3500 and 4000 THEN ''Limburg''
		WHEN job.[Bill-to Post Code] between 4000 and 5000 THEN ''Luik''
		WHEN job.[Bill-to Post Code] between 5000 and 6000 THEN ''Namen''
		WHEN job.[Bill-to Post Code] between 6000 and 6600 THEN ''Henegouwen''
		WHEN job.[Bill-to Post Code] between 6600 and 7000 THEN ''Luxemburg''
		WHEN job.[Bill-to Post Code] between 7000 and 8000 THEN ''Henegouwen''
		WHEN job.[Bill-to Post Code] between 8000 and 9000 THEN ''West-Vlaanderen''
		WHEN job.[Bill-to Post Code] between 9000 and 9999 THEN ''Oost-Vlaanderen''
		ELSE ''UNKNOWN''
	END
ELSE ''BUITENLAND''
END
,[Shift Code] as timesheet_dim1_shiftcode
,[Holiday] as timesheet_dim2_holiday
,[Sunday] as timesheet_dim3_sunday
,[Weekend] as timesheet_dim4_weekend
,[Resource No_] as timesheet_resource
,[Resource Competence] as resource_competence
,[Job Competence] as job_competence

-- Fact field calculation
,iif(trl.[Type Hour] <> 2, trl.[Quantity],0) as quantity_invoiced
,iif(trl.[Type Hour] <> 2, trl.[Unit Price],0) as unit_price
,iif(trl.[Type Hour] <> 2, trl.[Unit Price]*trl.[Quantity],0) as total_price
--,iif(trl.[Type Hour] <> 2 and trl.[Shift Code] = ''W'',trl.[Unit Cost Wait]*trl.[Quantity],trl.[Unit Cost Resource]*trl.[Quantity]) as resource_cost
,iif(trl.[Type Hour] <> 2, iif(trl.[Shift Code] = ''W'',trl.[Unit Cost Wait]*trl.[Quantity],trl.[Unit Cost Resource]*trl.[Quantity]),0	) as resource_cost
,iif(trl.[Type Hour] = 2 OR trl.[All In Job] = 1, trl.[Unit Cost KM]*trl.[No_ Of Resource KM]*2, 0) as unit_cost_km
,iif(trl.[Type Hour] = 2 OR trl.[All In Job] = 1, trl.[No_ Of Resource KM]*2, 0) as unit_km

FROM ' + @dsa_db_name + '.' + @dsa_schema + '.' + '[Detailed Timesheet Invoice] as TRL
LEFT JOIN '  + @dsa_db_name + '.' + @dsa_schema + '.' + '[job] as job
on job.[No_] = TRL.[Job No_]
Where  trl.[Job No_] <> ''''
-- AND trl.[Type Hour] <> 2
AND trl.[Invoice No_] like ''si%''
')
